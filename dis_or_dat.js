(function($) {
  Drupal.behaviors.dis_or_dat = {
    
    attach: function (context, settings) {
      var element = settings.dis_or_dat, id;
      for (id in element) {
        // Attach the receive callback
        element[id]['receive'] = this.sortableReceive;
        element[id]['over'] = this.sortableOver;
        element[id]['out'] = this.sortableOut;
        // Only run this once on unique elements.
        $('#' + id, context).once('dd-checkbox', function() {
          // Apply sortable.
          $(this).find('.dis-or-dat-dropzone').sortable(element[id]).disableSelection();
        });
      }
    },
    
    // Sortable's "receive" event callback
    sortableReceive: function (event, ui) {
      // if the item has landed in the "active-dropzone", tic the checkbox.
      if ($(event.target).hasClass('dis-or-dat-dropzone-active')) {
        $(ui.item).find('input').attr('checked', 'checked');
      }
      
      // Otherwise, remove the checkbox tic.
      else {
        $(ui.item).find('input').removeAttr('checked');
      }
    },
    
    // Sortable's "over" event callback
    sortableOver: function (event, ui) {
      $(event.target).addClass('hover');
    },
    
    sortableOut: function (event, ui) {
      $(event.target).removeClass('hover');
    }
    
  }
})(jQuery);