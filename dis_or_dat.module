<?php

/**
 * @file
 *   Form element for drag 'n' drop bling in your Drupal forms.
 *
 * Go to /dis_or_dat/test for the test page.
 * 
 * @todo remove the excessive styling.. We need defaults, but c'mon now..
 * @todo make this a field widget too!
 * @fixme the theme for the element has a lot of "responsibility" as far as
 * important classes. This is a problem because themer's may want to override
 * the theme, but not fully understand the importance of the classes.
 */

/**
 * Implements hook_theme().
 */
function dis_or_dat_theme($existing, $type, $theme, $path) {
  return array(
    'dis_or_dat' => array(
      'render element' => 'element',
    ),
  );
}


/**
 * Implements hook_element_info().
 */
function dis_or_dat_element_info() {
  $element['dis_or_dat'] = array(
    '#input' => TRUE,
    '#process' => array('form_process_checkboxes', 'dis_or_dat_form_process'),
    '#pre_render' => array('form_pre_render_conditional_form_element'),
    '#value_callback' => 'form_type_checkboxes_value',
    '#theme' => 'dis_or_dat',
  );

  return $element;
}

/**
 * Process callback
 */
function dis_or_dat_form_process($element) {
  $path = drupal_get_path('module', 'dis_or_dat');
  $settings['connectWith'] = '#' . $element['#id'] . ' .dis-or-dat-dropzone';
  $settings['revert'] = TRUE;
  $settings['cursor'] = 'move';
  
  // Scan the property list for applicable overrides.
  foreach (element_properties($element) as $property) {
    $match = '#sortable_';
    // If property exists, add it to the settings array.
    if (strpos($property, $match) !== FALSE) {
      $option = str_replace($match, '', $property);
      $settings[$option] = $element[$property];
    }
  }
  
  
  $element['#attached']['library'][] = array('system', 'ui.sortable');
  $element['#attached']['css'][] = $path . '/dis_or_dat.css';
  $element['#attached']['js'][] = $path . '/dis_or_dat.js';
  $element['#attached']['js'][] = array(
    'data' => array('dis_or_dat' => array($element['#id'] => $settings)),
    'type' => 'setting',
  );
  
  return $element;
}

/**
 * Theme function for DD Checkboxes.
 * 
 * @fixme I am so ashamed of this theme function. There is waaay to much
 * important stuff here that themers can totally mess up if they overriride the
 * theme.
 */
function theme_dis_or_dat($variables) {
  $element = $variables['element'];

  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  $attributes['class'][] = 'form-dis-or-dat';
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = array_merge($attributes['class'], $element['#attributes']['class']);
  }
  
  // Render active/inactive elements into variables
  $active = $inactive = '';
  foreach (element_children($element) as $child) {
    if (isset($element[$child]['#checked'])) {
      if ($element[$child]['#checked']) {
        $active .= drupal_render($element[$child]);
      }
      else {
        $inactive .= drupal_render($element[$child]);
      }
    }
  }
  
  $output  = '';
  $output .= '<div' . drupal_attributes($attributes) . '>';
  $output .= '<header class ="dis-or-dat-row">';
  $output .= '<span class="dis-or-dat-cell dis-or-dat-label">' . t('Active') . '</span> ';
  $output .= '<span class="dis-or-dat-cell dis-or-dat-label">' . t('Inactive') . '</span>';
  $output .= '</header>';
  $output .= '<div class="dis-or-dat-row dis-or-dat-dropzones">';
  $output .= '<section class="dis-or-dat-dropzone-active dis-or-dat-cell dis-or-dat-dropzone"> ' . $active . '</section>';
  $output .= '<section class="dis-or-dat-dropzone-inactive dis-or-dat-cell dis-or-dat-dropzone">' . $inactive . '</section>';
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}


/**
 * Implements hook_menu().
 */
function dis_or_dat_menu() {
  $items['dis_or_dat/test'] = array(
    'title' => 'testing dis_or_dat',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dis_or_dat_test'),
    'access callback' => TRUE,
  );
  
  return $items;
}

function dis_or_dat_test($form, &$form_state) {

  // Easy-peasy default implementation.
  $form['dis_or_dat_a'] = array(
    '#title' => "A",
    '#description' => "Easy-peasy default implementation.",
    '#type' => 'dis_or_dat',
    '#options' => drupal_map_assoc(array('A', 'B', 'C', 'D', 'E', 'F', 'G')),
    '#default_value' => variable_get('dis_or_dat_a', array()),
  );
  
  // This is disabled.
  $form['dis_or_dat_b'] = array(
    '#title' => "B",
    '#description' => "This is disabled.",
    '#type' => 'dis_or_dat',
    '#options' => drupal_map_assoc(array('X', 'Y', 'Z')),
    '#default_value' => variable_get('dis_or_dat_b', array()),
    '#sortable_disabled' => TRUE,
  );
  
  // It's /supposed/ to make a clone but I don't see it. 
  // Disables the smooth animation when releasing an option.
  // Using custom cursor.
  // Add an annoying delay before dragging starts
  $form['dis_or_dat_c'] = array(
    '#title' => "C",
    '#description' => "It's /supposed/ to make a clone but I don't see it. Disables the smooth animation when releasing an option. Using custom cursor. Add an annoying delay before dragging starts.",
    '#type' => 'dis_or_dat',
    '#options' => drupal_map_assoc(array('X', 'Y', 'Z')),
    '#default_value' => variable_get('dis_or_dat_c', array()),
    '#sortable_helper' => 'clone',
    '#sortable_revert' => FALSE,
    '#sortable_cursor' => 'no-drop',
    '#sortable_delay' => 300,
  );
  
  // Put grid in place for jaggedy-step dragging.
  // Using opacity for dragging item.
  // Using the pointer as the tolerence to detect droppable zones.
  // Disable scrolling from happening if item is dragged off screen.
  $form['dis_or_dat_d'] = array(
    '#title' => "D",
    '#description' => "Put grid in place for jaggedy-step dragging. Using opacity for dragging item. Using the pointer as the tolerence to detect droppable zones. Disable scrolling from happening if item is dragged off screen.",
    '#type' => 'dis_or_dat',
    '#options' => drupal_map_assoc(array('A', 'B', 'C', 'D', 'E', 'F', 'G')),
    '#default_value' => variable_get('dis_or_dat_d', array()),
    '#sortable_grid' => array('25', '25'),
    '#sortable_opacity' => 0.3,
    '#sortable_tolerance' => 'pointer',
    '#sortable_scroll' => FALSE,
    '#sortable_axis' => 'x',
  );
  
  // Using pointer tolerence because it's cool.
  // Forcing item to never leave its parent container, making it useless.
  // More use of transparency!
  $form['dis_or_dat_e'] = array(
    '#title' => "E",
    '#description' => "Using pointer tolerence because it's cool. Forcing item to never leave its parent container, making it useless. More use of transparency!",
    '#type' => 'dis_or_dat',
    '#options' => drupal_map_assoc(array('A', 'B', 'C', 'D')),
    '#default_value' => variable_get('dis_or_dat_e', array()),
    '#sortable_tolerance' => 'pointer',
    '#sortable_containment' => 'parent',
    '#sortable_opacity' => 0.6,
  );
  
  // Disable the first and last items from interacting
  // Add a class to the placeholder and ensure it has a size.
  $form['dis_or_dat_f'] = array(
    '#title' => "F",
    '#description' => "Disable the first and last items from interacting. Add a class to the placeholder and ensure it has a size.",
    '#type' => 'dis_or_dat',
    '#options' => drupal_map_assoc(array('A', 'B', 'C', 'D', 'E', 'F', 'G')),
    '#default_value' => variable_get('dis_or_dat_f', array()),
    '#sortable_items' => '> *:not(:last, :first)',
    '#sortable_placeholder' => 'ui-state-highlight',
    '#sortable_forcePlaceholderSize' => TRUE,
  );
  
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete test variables'),
    '#submit' => array('_dis_or_dat_delete_submit'),
  );
  
  return system_settings_form($form);
}

function _dis_or_dat_delete_submit($form, &$form_state) {
  foreach (array_keys($form_state['values']) as  $name) {
    if (strpos($name, 'dis_or_dat_') !== FALSE) {
      variable_del($name);
      drupal_set_message(t('%name has been deleted from variables table.', array('%name' => $name)));
    }
  }
}
